// include the library code:
#include <LiquidCrystal.h>

// portas digitais LCD
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// portas digitais
const int botaoDir = 6;
const int botaoEsq = 7;
const int setaEsq = 8;
const int setaDir = 9;
int farol = 10;
int reed = 13;

// portas analógicas
int ldr2 = A2;
int freio = A3;
int freioLed = A4;
int ldr1 = A5;

// variáveis
int estadoBotaoEsq = 0;
int estadoBotaoDir = 0;
int ldr1Valor = 0;
int ldr2Valor = 0;
int sensibilidade = 50;
int estadofreioLed = 0;
int estadoReed = 0;
int estadoAnteriorReed = 0;
int voltas = 0;
long tempoInicial = 0;
float tempoGasto = 0;

unsigned long tempoAnteriorEsq = 0;
bool piscarEsq = false;
int contEsq = 0;
int estadoSetaEsq = 0;

unsigned long tempoAnteriorDir = 0;
bool piscarDir = false;
int contDir = 0;
int estadoSetaDir = 0;

// constantes
// distancia percorrida em 1 volta do pneu
// considerando aro 26, cujo diâmetro é de 559mm = 0,559m
const float dist1volta = 3.14 * 0.559; // metros

void setup() {
  // mensagens iniciais do display LCD
  lcd.begin(16, 2);
  lcd.print("V: 0 km/h");
  lcd.setCursor(0, 1);
  lcd.print("D: 0 km");

  // inicializações de entrada
  pinMode(setaEsq, OUTPUT);
  pinMode(setaDir, OUTPUT);
  pinMode(farol, OUTPUT);
  pinMode(freioLed, OUTPUT);

  // inicializações de saída
  pinMode(botaoEsq, INPUT);
  pinMode(botaoDir, INPUT);
  pinMode(reed, INPUT);

  // inicialização da serial
  Serial.begin(9600);
}

void loop() {
  // odômetro / velocímetro
  estadoReed = digitalRead(reed);
  if (estadoReed == HIGH) {
    if (estadoAnteriorReed != estadoReed) {
      estadoAnteriorReed = estadoReed;

      voltas++;
      float distTotal = voltas * (dist1volta / 1000.0);

      int t = millis();
      float tempoGastoM = (t - tempoInicial); // milisegundos
      tempoGasto = tempoGastoM / 1000.0; // segundos
      tempoInicial = t;

      float velocInstantanea = 3.6 * (dist1volta / tempoGasto); // 3,6*(m/s) = km/h

      lcd.setCursor(3, 0);
      lcd.print(velocInstantanea, 2);
      lcd.print(" km/h");
      lcd.setCursor(3, 1);
      lcd.print(distTotal, 2);
      lcd.print(" km");
    }
  } else {
    estadoAnteriorReed = estadoReed;
  }

  // freio
  estadofreioLed = analogRead(freio);
  if (estadofreioLed > 0) {
    digitalWrite(freioLed, HIGH);
  } else {
    digitalWrite(freioLed, LOW);
  }

  // seta para esquerda
  estadoBotaoEsq = digitalRead(botaoEsq);
  if (estadoBotaoEsq == HIGH) {
    piscarEsq = true;
  }

  unsigned long tempoEsq = millis();
  if (piscarEsq) {
    if (contEsq < 6) {
      if (tempoEsq - tempoAnteriorEsq >= 500) {
        tempoAnteriorEsq = tempoEsq;
        estadoSetaEsq = digitalRead(setaEsq);
        if (estadoSetaEsq == LOW) {
          estadoSetaEsq = HIGH;
        } else {
          estadoSetaEsq = LOW;
        }
        contEsq++;
        digitalWrite(setaEsq, estadoSetaEsq);
      }
    } else {
      piscarEsq = false;
      contEsq = 0;
    }
  }

  // seta para direita
  estadoBotaoDir = digitalRead(botaoDir);
  if (estadoBotaoDir == HIGH) {
    piscarDir = true;
  }

  unsigned long tempoDir = millis();
  if (piscarDir) {
    if (contDir < 6) {
      if (tempoDir - tempoAnteriorDir >= 500) {
        tempoAnteriorDir = tempoDir;
        estadoSetaDir = digitalRead(setaDir);
        if (estadoSetaDir == LOW) {
          estadoSetaDir = HIGH;
        } else {
          estadoSetaDir = LOW;
        }
        contDir++;
        digitalWrite(setaDir, estadoSetaDir);
      }
    } else {
      piscarDir = false;
      contDir = 0;
    }
  }

  // farol
  ldr1Valor = analogRead(ldr1);
  ldr2Valor = analogRead(ldr2);
  if (ldr1Valor < sensibilidade && ldr2Valor < sensibilidade) {
    digitalWrite(farol, HIGH);
  } else {
    digitalWrite(farol, LOW);
  }
}




